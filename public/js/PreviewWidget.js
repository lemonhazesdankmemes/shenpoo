"use strict";

const PreviewWidget = (function() {

	this.MEM = {
		clock : new THREE.Clock()
	}

	this.DOM = {
		section : document.getElementById('PreviewWidget.section')
	}

	this.EVT = {
		handleWindowResize : evt_handleWindowResize.bind(this)
	}

	this.API = {
		animate : api_animate.bind(this),
		loadModel : api_loadModel.bind(this),
		addLabels : api_addLabels.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		this.MEM.scene = new THREE.Scene();
		this.MEM.scene.background = new THREE.Color(0x3c3c3c);

		this.MEM.renderer = new THREE.WebGLRenderer({
			antialias: true
		});

		let width = window.innerWidth - 400;
		let height = window.innerHeight - 36;
		this.MEM.renderer.setPixelRatio(window.devicePixelRatio);
		this.MEM.renderer.setSize(width, height);
		this.DOM.section.appendChild(this.MEM.renderer.domElement);

		this.MEM.camera = new THREE.PerspectiveCamera(60, width / height, 0.01, 1000);
		this.MEM.camera.position.set(-1, 0.8, -2);
		this.MEM.camera.lookAt(new THREE.Vector3(0,1,0));

		this.MEM.controls = new THREE.OrbitControls(this.MEM.camera, this.DOM.section);
		this.MEM.controls.enableDamping = true;
		this.MEM.controls.dampingFactor = 0.25;
		this.MEM.controls.screenSpacePanning = false;

		let grid = new THREE.GridHelper(10, 10, 0xff0000, 0xffffff);
		grid.position.y = -0.8;
		this.MEM.scene.add(grid);
		// this.MEM.scene.add(new THREE.AmbientLight(0xcccccc));
		
		const aLight = new THREE.DirectionalLight( 0xffffff, 0.9 );
		aLight.position.x = -5;
		aLight.position.y = 3;
		aLight.position.z = 3;
		this.MEM.scene.add( aLight );
		
		window.addEventListener('resize', this.EVT.handleWindowResize);

		this.API.animate();
		this.API.loadModel();
		this.API.addLabels();

	}

	function api_addLabels() {

		console.log("add the labels");

		let ng = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const nm = new THREE.MeshLambertMaterial( {color: 0xffff00} );
		const north = new THREE.Mesh( ng, nm );
		north.position.z = -2;
		north.position.y = -0.7;
		this.MEM.scene.add(north);

		let sg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const sm = new THREE.MeshLambertMaterial( {color: 0x0000ff} );
		const south = new THREE.Mesh( sg, sm );
		south.position.z = 2;
		south.position.y = -0.7;
		this.MEM.scene.add(south);

		let wg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const wm = new THREE.MeshLambertMaterial( {color: 0xff0000} );
		const west = new THREE.Mesh( wg, wm );
		west.position.x = -2;
		west.position.y = -0.7;
		this.MEM.scene.add(west);

		let eg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const em = new THREE.MeshLambertMaterial( {color: 0x00ff00} );
		const east = new THREE.Mesh( eg, em );
		east.position.x = 2;
		east.position.y = -0.7;
		this.MEM.scene.add(east);


	}

	function evt_handleWindowResize() {

		console.log("window resize");

		let width = window.innerWidth - 400;
		let height = window.innerHeight - 36;
		this.MEM.renderer.setSize(width, height);

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();

	}

	function api_loadModel() {

		// First Load the textures (lazy)

		let mats = [];
		for(let i = 0; i < 10; i++) {
			let path = `assets/ryo_0${i}.png`;
			let texture = new THREE.TextureLoader().load(path);
			let material = new THREE.MeshBasicMaterial({
				map : texture,
				skinning : true
			});
			mats.push(material);
		}

		// Then we go ahead and load in the binary

		let ajax = new XMLHttpRequest();
		ajax.open('GET', 'assets/ryo.mt5');
		ajax.responseType = 'arraybuffer';
		ajax.send();

		ajax.onload = () => {
			
			let data = ajax.response;
			let loader = new Mt5Loader(mats, data);
			let mesh = loader.parse();
			mesh.position.y = -0.8;

			let skelHelper = new THREE.SkeletonHelper(mesh);
			skelHelper.material.lineWidth = 5;
			this.MEM.scene.add(skelHelper);
			this.MEM.scene.add(mesh);

			SelectWidget.API.loadAnimation(242);

		}

	}

	function api_animate() {

		window.requestAnimationFrame(this.API.animate);

		let delta = this.MEM.clock.getDelta();
		if(this.MEM.mixer) {
			this.MEM.mixer.update(delta);
		}

		this.MEM.controls.update();
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);


	}


}).apply({});
