"use strict";

const TableWidget = (function() {

	this.MEM = {}

	this.DOM = {
		thead : document.getElementById('TableWidget.thead'),
		tbody : document.getElementById('TableWidget.tbody')
	}

	this.EVT = {}

	this.API = {
		getBoneName : api_getBoneName.bind(this),
		fillInValues : api_fillInValues.bind(this),
		interpolateLinear : api_interpolateLinear.bind(this),
		interpolateBezier : api_interpolateBezier.bind(this),
		renderTable : api_renderTable.bind(this)
	}

	init.apply(this);
	return this;

	function init() {


	}

	function api_getBoneName(id, type) {

		for(let key in MotnNames) {
			if(MotnNames[key].id !== id) {
				continue;
			}

			if(MotnNames[key].type !== type) {
				continue;
			}

			let bone = MotnNames[key];
			bone.name = key;
			return bone;
		}

		return null;

	}


	function api_fillInValues (length, motn) {
		
		this.MEM.length = length;
		this.MEM.motn = motn;
		
		// First we go ahead and flatten everything

		let flat = new Array();
		this.MEM.motn.forEach( mot => {
			
			let bone = this.API.getBoneName(mot.motn_id, mot.type);

			mot.axis.forEach( d => {
				
				let f = {
					motn_id : mot.motn_id,
					type : mot.type,
					axis : d,
					frames : mot[d],
					bone : bone
				};

				flat.push(f);

			});

		});
		
		this.MEM.flat = flat;
		this.API.interpolateLinear();
		this.API.renderTable();

	}

	function api_interpolateLinear() {

		this.MEM.flat.forEach(flat => {
			
			/*
			if(flat.motn_id !== 8) {
				return;
			}
			*/

			let array = new Array(this.MEM.length);
			for(let i = 0; i < flat.frames.length; i++) {
				array[flat.frames[i].frame] = flat.frames[i].value;
			}
			
			let val_count = 0;
			for(let i = 0; i < array.length; i++) {
				if(typeof array[i] === 'undefined') {
					continue;
				}
				val_count++;
			}

			if(val_count < 2) {
				return;
			}
			
			while(1) {
				
				let a, b;
				for(let i = 0; i < array.length; i++) {
					if(typeof(array[i]) !== 'undefined') {
						a = {
							index : i,
							value : array[i]
						}
					} else if(typeof(array[i]) === 'undefined' && a) {
						break;
					}
				}

				if(!a) {
					break;
				}
				console.log("A");
				console.log(a);

				for(let i = a.index + 1; i < array.length; i++) {
					if(typeof(array[i]) === 'undefined') {
						continue;
					}

					b = {
						index : i,
						value : array[i]
					}
					break;
				}
				
				if(!b) {
					break;
				}
				console.log("B");
				console.log(b);

				let f_diff = b.index - a.index;
				let v_diff = b.value - a.value;
				let diff = v_diff / f_diff;
				let val = a.value;

				for(let i = a.index; i < b.index; i++) {
					array[i] = val;
					val += diff;
				}

				console.log();

			}

			console.log(array);
			
			let clean = [];
			for(let i = 0; i < array.length; i++) {
				clean.push({
					frame : i,
					val : array[i]
				});
			}

			for(let i = 0; i < flat.frames.length; i++) {
				console.log("do the thing!!");

				if('value' in flat.frames[i]) {
					clean[flat.frames[i].frame].value = flat.frames[i].value;
				}

				if('easing' in flat.frames[i]) {
					clean[flat.frames[i].frame].easing = flat.frames[i].easing;
				}

			}
			
			console.log(clean);
			flat.frames = clean;

		});

	}

	function api_interpolateBezier() {

	}

	function api_renderTable() {

		this.DOM.thead.innerHTML = '';
		this.DOM.tbody.innerHTML = '';

		// First we create the header

		let rh = this.DOM.thead.insertRow();
		let h0 = rh.insertCell();
		let h1 = rh.insertCell();
		let h2 = rh.insertCell();
		let h3 = rh.insertCell();
		
		h0.textContent = 'MTON';
		h1.textContent = 'MT5';
		h2.textContent = 'Axis';
		h3.textContent = 'Type';

		for(let i = 0; i < this.MEM.length; i++) {
			rh.insertCell().textContent = i;
		}

		this.MEM.flat.forEach( flat => {

			let row = this.DOM.tbody.insertRow();
			let c0, c1, c2, c3;

			if(flat.axis === 'x') {
				c0 = row.insertCell();
				c1 = row.insertCell();
				c2 = row.insertCell();
				c3 = row.insertCell();
			} else {
				c1 = row.insertCell();
				c2 = row.insertCell();
			}
			
			if(flat.bone) {
				c1.textContent = flat.bone[flat.axis];
			}
			c2.textContent = flat.axis;

			if(flat.axis === 'x') {

				if(flat.bone) {
					let parts = flat.bone.name.split(' ');
					for(let i = 0; i < parts.length; i++) {
						let a = document.createElement('span');
						a.textContent = parts[i];
						c0.appendChild(a);
					}
				}
				
				let b = document.createElement('span');
				b.textContent = flat.motn_id;
				c0.appendChild(b);

				c3.textContent = flat.type.toUpperCase();
				c0.setAttribute('rowspan', '3');
				c3.setAttribute('rowspan', '3');

				if(flat.bone) {
					let c = document.createElement('span');
					c.textContent = flat.bone.format;
					c3.appendChild(c);

					if(flat.bone.format === 'IK') {
						c3.classList.add('ik');
					}
				}

			}


			for(let i = 0; i < this.MEM.length; i++) {
				let cell = row.insertCell();

				if(flat.axis === 'x') {
					cell.classList.add('dash-x');
				} else if(flat.axis === 'y') {
					cell.classList.add('dash-y');
				}
				
				let found = flat.frames[i];
				if(flat.motn_id === 8) {
					console.log(found);
				}

				if(!found) {
					continue;
				}
				
				if(found.value) {
					if(flat.type === 'pos') {
						cell.textContent = found.value.toFixed(2);
					} else {
						let rad = found.value % (Math.PI*2);
						cell.textContent = rad.toFixed(2);
					}
					cell.classList.add('val');
				} else if(found.val) {
				
					if(flat.type === 'pos') {
						cell.textContent = found.val.toFixed(2);
					} else {
						let rad = found.val % (Math.PI*2);
						cell.textContent = rad.toFixed(2);
					}

				}

				if(found.easing) {
					let str = [];
					str.push(found.easing[0].toFixed(2));
					str.push(found.easing[1].toFixed(2));
					cell.setAttribute('title', '(' + str.join(',') + ')');
					cell.classList.add('ease');
				}

			}

		});

	}

}).apply({});
