"use strict";

const SelectWidget = (function () {

	this.MEM = {}

	this.DOM = {
		text : document.getElementById('SelectWidget.text'),
		select : document.getElementById('SelectWidget.select')
	}

	this.EVT = {
		handleSelectChange : evt_handleSelectChange.bind(this)
	}

	this.API = {
		fetchMotion : api_fetchMotion.bind(this),
		readMotion : api_readMotion.bind(this),
		parseAnimation : api_parseAnimation.bind(this),
		loadAnimation : api_loadAnimation.bind(this),
		decodeFloat16 : api_decodeFloat16.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		this.API.fetchMotion();
		this.DOM.select.addEventListener('change', this.EVT.handleSelectChange);

	}

	function api_parseAnimation(anim) {
		
		this.MEM.ofs = anim.dataOfs;

		// Read the header

		const motn = [];
		let length = this.MEM.view.getUint16(this.MEM.ofs + 0x00, true);
		let header = {
			block1Ofs : 0x0c,
			block2Ofs : this.MEM.view.getUint16(this.MEM.ofs + 0x04, true),
			block3Ofs : this.MEM.view.getUint16(this.MEM.ofs + 0x06, true),
			block4Ofs : this.MEM.view.getUint16(this.MEM.ofs + 0x08, true),
			block5Ofs : this.MEM.view.getUint16(this.MEM.ofs + 0x0a, true)
		}
		
		let block2EntryHalfSize = length & 0xFFFF8000 ? false : true;
		let block3EntryHalfSize = (length & 0x7FFF) <= 0xFF ? true : false;

		// Read Block 1
		
		const pos_mask = 0x1c0;
		const rot_mask = 0x38;

		let instruction;
		let axis_count = 0;
		let index = 0;
		this.MEM.ofs = anim.dataOfs + header.block1Ofs;
		instruction = this.MEM.view.getUint16(this.MEM.ofs, true);
		while((index < 127) && instruction !== 0) {
			let motn_id = instruction >> 9;
			
			if(instruction & pos_mask) {
				const node = {
					motn_id : motn_id,
					type : 'pos',
					axis : []
				}

				if(instruction & 0x100) {
					node.axis.push('x');
					axis_count++;
				}

				if(instruction & 0x80) {
					node.axis.push('y');
					axis_count++;
				}

				if(instruction & 0x40) {
					node.axis.push('z');
					axis_count++;
				}

				motn.push(node);
			} 

			if(instruction & rot_mask) { 
				const node = {
					motn_id : motn_id,
					type : 'rot',
					axis : []
				}

				if(instruction & 0x20) {
					node.axis.push('x');
					axis_count++;
				}

				if(instruction & 0x10) {
					node.axis.push('y');
					axis_count++;
				}

				if(instruction & 0x08) {
					node.axis.push('z');
					axis_count++;
				}

				motn.push(node);

			}
			
			this.MEM.ofs += 2;
			index++;
			instruction = this.MEM.view.getUint16(this.MEM.ofs, true);
			
		}

		// Read Block 2

		let block2Size = header.block3Ofs - header.block2Ofs;
		// If block 2 is half size, these should be the same

		this.MEM.ofs = anim.dataOfs + header.block2Ofs;
		motn.forEach(node => {
			
			node.axis.forEach( d => {
				let size;
				if(block2EntryHalfSize) {
					size = this.MEM.view.getUint8(this.MEM.ofs);
					this.MEM.ofs++;
				} else {
					size = this.MEM.view.getUint16(this.MEM.ofs, true);
					this.MEM.ofs += 2;
				}
				node[d] = new Array(size);
			});
		});

		// Read Block 3
		
		let zero_count = 0;
		let total_frames = 0;

		let start_ofs = anim.dataOfs + header.block3Ofs;
		let end_ofs = anim.dataOfs + header.block4Ofs;

		for(let i = start_ofs; i < end_ofs; i++) {
			let byte = this.MEM.view.getUint8(i);
			if(i !== 0) {
				continue;
			}
			zero_count++;
		}

		this.MEM.ofs = anim.dataOfs + header.block3Ofs;
		motn.forEach(node => {
			
			node.axis.forEach( d => {
				
				for(let i = 0; i < node[d].length; i++) {
					
					let no;
					if(block3EntryHalfSize) {
						no = this.MEM.view.getUint8(this.MEM.ofs);
						this.MEM.ofs++;
					} else {
						no = this.MEM.view.getUint16(this.MEM.ofs, true);
						this.MEM.ofs += 2;
					}

					node[d][i] = { frame : no };

				}

				node[d].unshift({frame:0});
				node[d].push({frame:length - 1});
				total_frames += node[d].length;

			});

		});

		let block4Size = header.block5Ofs - header.block4Ofs;

		// Read Block 4

		this.MEM.ofs = anim.dataOfs + header.block4Ofs;

		let b4 = 0;
		motn.forEach(node => {
			
			node.axis.forEach( d => {
				
				let index = 0;

				do {
					let countLookupTable =  [0,  1,  2,  3,  1,  2,  3,  4,  2,  3,  4,  5,  3,  4,  5,  6,
											   1,  2,  3,  4,  2,  3,  4,  5,  3,  4,  5,  6,  4,  5,  6,  7,
											   2,  3,  4,  5,  3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,
											   3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,
											   1,  2,  3,  4,  2,  3,  4,  5,  3,  4,  5,  6,  4,  5,  6,  7,
											   2,  3,  4,  5,  3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,
											   3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,
											   4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,  7,  8,  9,  10,
											   2,  3,  4,  5,  3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,
											   3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,
											   4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,  7,  8,  9,  10,
											   5,  6,  7,  8,  6,  7,  8,  9,  7,  8,  9,  10, 8,  9,  10, 11,
											   3,  4,  5,  6,  4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,
											   4,  5,  6,  7,  5,  6,  7,  8,  6,  7,  8,  9,  7,  8,  9,  10,
											   5,  6,  7,  8,  6,  7,  8,  9,  7,  8,  9,  10, 8,  9,  10, 11,
											   6,  7,  8,  9,  7,  8,  9,  10, 8,  9,  10, 11, 9,  10, 11, 12];		
					let keyframeBlockType = this.MEM.view.getUint8(this.MEM.ofs);
					console.log("keyframeBlockType: 0x%s (%s)", keyframeBlockType.toString(16), keyframeBlockType.toString(2));

					let keyframeBlockSize = countLookupTable[keyframeBlockType];
					console.log("keyframeBlockSize: 0x%s (%s)", keyframeBlockSize.toString(16), keyframeBlockSize.toString(2));


					b4++;
					this.MEM.ofs++;

					/*if(keyframeBlockType === 0) {
						break;
					}*/

					//if(keyframeBlockType & 0xc0) 
					{
						if(keyframeBlockType & 0x80) {
							node[d][index].high = true;
						}

						if(keyframeBlockType & 0x40) {
							node[d][index].low = true;
						}

					}
					index++;

					//if(keyframeBlockType & 0x30) 
					{
						if(keyframeBlockType & 0x20) {
							node[d][index].high = true;
						}

						if(keyframeBlockType & 0x10) {
							node[d][index].low = true;
						}

					}
					index++;

					//if(keyframeBlockType & 0x0c) 
					{
						if(keyframeBlockType & 0x08) {
							node[d][index].high = true;
						}

						if(keyframeBlockType & 0x04) {
							node[d][index].low = true;
						}

					}
					index++;

					//if(keyframeBlockType & 0x03) 
					{
						if(keyframeBlockType & 0x02) {
							node[d][index].high = true;
						}

						if(keyframeBlockType & 0x01) {
							node[d][index].low = true;
						}

					}
					index++;

				} while(index < node[d].length);

			});

		});
		
		// Read Block 5

		this.MEM.ofs = anim.dataOfs + header.block5Ofs;
		motn.forEach(node => {
			
			node.axis.forEach( d => {
				
				node[d].forEach(frame => {
					
					if(frame.high) {
						let a = this.MEM.view.getUint16(this.MEM.ofs, true);
						a = this.API.decodeFloat16(a);
						this.MEM.ofs += 2;
						let b = this.MEM.view.getUint16(this.MEM.ofs, true);
						b = this.API.decodeFloat16(b);
						this.MEM.ofs += 2;
						frame.easing = [a, b];
					}

					if(frame.low) {
						let a = this.MEM.view.getUint16(this.MEM.ofs, true);
						a = this.API.decodeFloat16(a);
						if(node.type === 'rot') {
							a *= 65536;
							a = a % (Math.PI * 2);
						}
						this.MEM.ofs += 2;
						frame.value = a;

					}
					delete frame.high;
					delete frame.low;

				});

			});

		});
		
		this.DOM.text.textContent = JSON.stringify(motn, null, 4);
		TableWidget.API.fillInValues(length, motn);

	}

	function api_decodeFloat16(binary) {

		let exponent = (binary & 0x7C00) >> 10;
		var fraction = binary & 0x03FF;

		return (binary >> 15 ? -1 : 1) * (
			exponent ?
			(
				exponent === 0x1F ?
				fraction ? NaN : Infinity :
				Math.pow(2, exponent - 15) * (1 + fraction / 0x400)
			) : 6.103515625e-5 * (fraction / 0x400)
		);

	}

	function api_loadAnimation(index) {
		
		if(!this.MEM.seq_list) {
			return this.MEM.load_in = index;
		}

		let anim = this.MEM.seq_list[index];
		this.DOM.select.selectedIndex = index + 1;
		this.API.parseAnimation(anim);

	}

	function evt_handleSelectChange() {
	
		let index = parseInt(this.DOM.select.value);
		let anim = this.MEM.seq_list[index];
		this.API.parseAnimation(anim);

	}

	function api_fetchMotion() {

		const ajax = new XMLHttpRequest();
		ajax.open('GET', 'assets/motion.bin');
		ajax.responseType = 'arraybuffer';
		ajax.send();

		ajax.onload = () => {
			
			this.MEM.buffer = ajax.response;
			this.MEM.view = new DataView(ajax.response);
			this.API.readMotion();

		}

	}

	function api_readMotion() {

		const header = {
			sequence_table_ofs : this.MEM.view.getUint32(0x00, true),
			sequence_names_ofs : this.MEM.view.getUint32(0x04, true),
			sequence_data_ofs : this.MEM.view.getUint32(0x08, true),
			sequence_count : this.MEM.view.getUint32(0x0c, true),
			filelength : this.MEM.view.getUint32(0x10, true)
		}

		const SEQ_LIST = new Array(header.sequence_count-1);

		let ofs_a = header.sequence_table_ofs;
		let ofs_b = header.sequence_names_ofs;

		for(let i = 0; i < SEQ_LIST.length; i++) {

			SEQ_LIST[i] = {
				dataOfs : this.MEM.view.getUint32(ofs_a + 0x00, true),
				ptr_b : this.MEM.view.getUint32(ofs_a + 0x04, true),
				name : ''
			}
			ofs_a += 8;

			let name_ptr = this.MEM.view.getUint32(ofs_b, true);
			ofs_b += 4;

			let ch;
			while( (ch = this.MEM.view.getUint8(name_ptr++)) !== 0) {
				 SEQ_LIST[i].name += String.fromCharCode(ch);
			}
			let num = (i).toString();
			while(num.length < 3) {
				num = "0" + num;
			}
			SEQ_LIST[i].dataOfs += header.sequence_data_ofs;
			const option = document.createElement('option');
			option.textContent = num + ") " + SEQ_LIST[i].name;
			option.value = i;
			this.DOM.select.appendChild(option);
		}

		this.MEM.seq_list = SEQ_LIST;

		if(this.MEM.load_in) {
			this.API.loadAnimation(this.MEM.load_in);
			delete this.MEM.load_in;
		}

	}

}).apply({});
